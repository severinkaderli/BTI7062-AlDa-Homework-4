---
title: "Homework Task 4"
author: [Severin Kaderli, Alan Kilic, Marius Schär]
date: "2018-09-26"
titlepage: true
toc-own-page: true
toc-title: "Inhaltsverzeichnis"
header-includes: |
     \usepackage{unicode-math}
...

# Task 1
**Prove non-reflexivity (for $o$ and $\omega$):** proving non-reflexivity is relatively simple. A Relation being reflexive means that every elemement ($a$) has a given relation ($\mathbb{R}$) with itself as well: $a \mathbb{R} a$.

**The case of $o$:**
$$
\forall f(0),c > 0 : f(0) \mathbb{R} c \cdot f(0) \implies f(0) > c \cdot f(0)
$$

Because $a < c \cdot a$ is never true except if $c = 0$, little $o$ is not reflexive. 

**The case of $\omega$:**
$$
\forall f(0),c > 0 : f(0) \mathbb{R} c \cdot f(0) \implies f(0) < c \cdot f(0)
$$

Because $a > c \cdot a$ is never true except if $c = 0$, little $\omega$ is not reflexive. 

**Prove transitivity (for $o$ and $\omega$):** in this section, we prove that the relation $\mathbb{R} : \text{ is } \omega() \text{ respecitively is } o()$ is transitive. We shorten $f()$ to $f$.

**The case of $\omega$:**
The relation in question is: $x \mathbb{R} y \implies x \text{ is } \omega(y)$. We thus need to prove:

$$
\forall f \forall g \forall h : \text{ if } f \mathbb{R} g \land g \mathbb{R} h \implies f \mathbb{R} h
$$

thus assume that

$$
\begin{aligned}
  f \; \mathbb{R} \; g &\land g \; \mathbb{R} \; h\\
  f > c_{1} \cdot g &\land g > c_{2} \cdot h\\
                    &\quad\, c_{1} \cdot g > c_{1} \cdot c_{2} \cdot h\\
  f > c_{1} \cdot c_{2} \cdot h &
\end{aligned}
$$

we can replace the product of $c_{1} \cdot c_{2}$ with another constant $c_{3}$, thus it follows that

$$
\exists c_{3} (f(n) > c_{3} \cdot h(n))
$$

to prove

$$
f \text{ is } \omega(h)
$$

**The case of $o$:** Because $<$ is (from the perspective of transitivity) the same as $>$ it follows trivially and immediately that the same is also true for $f$ is $o(h)$.

# Task 2
In Task 1 we already proved that $<$ and $>$ are transitive Relations.

Because the relation $\mathbb{R} :x \text{ is } \Omega(y)$ boils down to $x \leq c \cdot y$,

we only need to prove that $\exists(c \in \mathbb{R}_{>0})(x = c \cdot y)$.

To prove *reflexivity*, we need to prove that $x \text{ is } \Omega(x) \implies x \leq c \cdot x$

It's immediately obvious that $\exists(c \in \mathbb{R}_{>0})$, because the solution is $c = 1$, resulting in $x = 1 \cdot x$.

The same follows trivially for $\mathbb{R} :x \text{ is } \mathcal{O}(y) \implies x \geq c \cdot y$, because the relevant sub-relation is still $=$.

# Task 4
We prove that $\Theta$ is symmetric by using the definition of an asymmetric relation and negating it (proof by contraposition).

$$
\begin{aligned}
  \forall(f, g)\quad& f\mathbb{R}g \enskip \to \enskip \lnot(g\mathbb{R}f) \\
  \lnot(\forall(f, g)\enskip& \lnot(f\mathbb{R}g) \enskip \lor \enskip  \lnot(g\mathbb{R}f)) \\
  \exists(f, g)\quad& f\mathbb{R}g \enskip \land \enskip g\mathbb{R}f \\
  \exists(f, g)\quad& f\text{ is } \Theta(g) \enskip \land \enskip g\text{ is } \Theta(f) \\
  \exists(f,g)\quad& c_{1} \cdot g \ge f \ge c_{2} \cdot g \enskip \land \enskip c_{1} \cdot f \ge g \ge c_{2} \cdot f
\end{aligned}
$$

The proof is given when $c_{1}$ and $c_{2}$ are $1$.

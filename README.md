# BTI7062-AlDa-Homework-4
## Tasks

1. every one of our little notions of asymptotic complexity
    define a relation that is not reflexive (which is different from
    irreflexive!) by giving a counter-example, but transitive (a
    so-called strict preorder )
2. every one of our big notions of asymptotic complexity
    defines a relation that is reflexive and transitive (a so-called
    preorder )
3. every one of our little and big notions of asymptotic
    complexity defines a relation that is additive and
    multiplicative (facultative subtask, difficult)
4. Θ defines a symmetric (and thus an equivalence) relation

## PDF
The ready to submit PDF file can be found [here](https://gitlab.com/severinkaderli/BTI7062-AlDa-Homework-4/builds/artifacts/master/raw/TEAM-Kaderli-Kilic-Schaer.pdf?job=PDF).